function ExtendReach(target,resources)
{
	this.target = target;
	this.spritesAr = new Array();
	this.soundWavesAr = new Array();
	this.resources = resources;
	this.setUpPreloader();
}

ExtendReach.prototype = {
	
	setUpPreloader: function()
	{
		this.keepAspectRatio = true;
		this.loadedGraphics = 0;
		canvas = document.getElementById("extendReach");
		stage = new createjs.Stage(canvas);
		stage.enableMouseOver(10);
		this.stage = stage
		borderPadding = 10;

		var barHeight = 20;
		loaderColor = createjs.Graphics.getRGB(61, 92, 155);
		loaderBar = new createjs.Container();

		bar = new createjs.Shape();
		bar.graphics.beginFill(loaderColor).drawRect(0, 0, 1, barHeight).endFill();

		imageContainer = new createjs.Container();
		imageContainer.x = 430;
		imageContainer.y = 200;

		loaderWidth = 300;
		stage.addChild(imageContainer);

		var bgBar = new createjs.Shape();
		var padding = 3
		bgBar.graphics.setStrokeStyle(1).beginStroke(loaderColor).drawRect(-padding / 2, -padding / 2, loaderWidth + padding, barHeight + padding);

		loaderBar.x = canvas.width - loaderWidth >> 1;
		loaderBar.y = canvas.height - barHeight >> 1;
		loaderBar.addChild(bar, bgBar);

		stage.addChild(loaderBar);

		manifest = 
		[
			{src: this.resources.image1, id: "image1"},
			{src: this.resources.image2, id: "image2"},
			
		];
		this.map = new Array();
		preload = new createjs.LoadQueue(true);
       

		preload.on("progress", this.handleProgress,this);
		preload.on("complete", this.handleComplete,this);
		preload.on("fileload", this.handleFileLoad,this);
		preload.loadManifest(manifest,true);
		createjs.Ticker.setFPS(30);
	},
	
	handleProgress: function(event) {
		bar.scaleX = event.loaded * loaderWidth;
	},

	handleComplete: function(event) {
		
		loaderBar.visible = false;
		
	   	this.setupAnimation();
	   	
       	stage.update();
       	var self = this;
       	createjs.Ticker.addEventListener("tick",self.handleTick);
       	console.log("animation in")
	    
	},

    handleTick: function(event){
    	stage.update();
    },

	handleFileLoad: function(event) {
		this.map.push(new createjs.Bitmap(event.result))
		stage.update();
	},

    setupAnimation: function()
    {
    	this.delaysAr = new Array();
    	this.delaysAr = [100,500,700,1000,2000];
    	this.alphaAmt = new Array();
    	this.alphaAmt = [1,1,.4,1,1];
    	this.spritesAr=[this.map[0],this.map[1]];
		stage.addChild(this.spritesAr[0]);
		this.spritesAr[1].x = this.resources.width/2;
		stage.addChild(this.spritesAr[1]);
    	
    	var lid = new createjs.Shape();
    	this.lidContainer = new createjs.Container();
    	lid.graphics.beginFill('#000000').drawRect(0, 0, this.resources.width, this.resources.height);
    	this.lidContainer.addChild(lid);
    	this.lidContainer.alpha = .5;
    	this.spritesAr.push(this.lidContainer);
    	this.textContainer = new createjs.Container();
    	this.spritesAr.push(this.textContainer);
    	stage.addChild(this.lidContainer);

	    var title = new createjs.Text(this.resources.title, "40px freight-sans-pro","#ffffff");
	    this.textContainer.addChild(title);
		this.textContainer.x=100;
		this.textContainer.y=50
		this.textContainer.origins = {x:this.textContainer.x,y:this.textContainer.y};
		title.textBaseline = "alphabetic";
		stage.addChild(this.textContainer);
    	stage.update();
    	
    	this.soundWavePos = new Array();
    	this.soundWavePos = [{color:'#ffffff',x:220,y:165,rotation:23},{color:'#aaed5d',x:490,y:190,rotation:245},{color:'#a558d5',x:590,y:345,rotation:245},{color:'#f79537',x:700,y:250,rotation:245}]
    	for(var i = 0;i<this.soundWavePos.length;i++)
    	{
			var temp = new SoundWave();
	    	temp.createBg(this.soundWavePos[i].color,this.soundWavePos[i].rotation,i);
	    	stage.addChild(temp);
	    	temp.x = this.soundWavePos[i].x;
	    	temp.y = this.soundWavePos[i].y;
	    	this.soundWavesAr.push(temp);
	    	temp.alpha = 0;
    	}
    	this.playAnimation();
    	//
    	
    	//
    },

    resetAnimation: function()
    {
    	console.log("reset animation")
    	var self = this;
       	createjs.Ticker.addEventListener("tick",self.handleTick);
       	for(var i=0;i<this.spritesAr.length;i++)
		{
			this.spritesAr[i].alpha = 0;
			createjs.Tween.get(this.spritesAr[i])
	         .wait(this.delaysAr[i])
	         .to({alpha:this.alphaAmt[i]}, 1000,createjs.Ease.linear)
		}
    },

	playAnimation: function()
	{
		
		this.textContainer.alpha = 0;
		for(var i=0;i<this.spritesAr.length;i++)
		{
			this.spritesAr[i].alpha = 0;
			createjs.Tween.get(this.spritesAr[i])
	         .wait(this.delaysAr[i])
	         .to({alpha:this.alphaAmt[i]}, 1000,createjs.Ease.linear)
		}
		
		for(var i=0;i<this.soundWavesAr.length;i++)
		{
			
			this.soundWavesAr[i].alpha = 0;
			createjs.Tween.get(this.soundWavesAr[i])
	         .wait(1000)
	         .to({alpha:1}, 1000,createjs.Ease.linear)
		}
	},

	animationComplete: function(event)
	{
		
		//createjs.Ticker.on("tick", myFunction);
		//function myFunction(event) {
		   // event.remove();
		//}
		//debugger
		//createjs.Ticker.removeEventListener("tick", handleTick);
		var self = this;
       	createjs.Ticker.removeEventListener("tick",self.handleTick);
	},
	resize: function(w,h)
	{
	
	var ow = 858; // your stage width
	var oh = 457; // your stage height

	if (this.keepAspectRatio == true)
	{
	    // keep aspect ratio
	    var scale = Math.min(w / ow, h / oh);
	    stage.scaleX = scale;
	    stage.scaleY = scale;

	   // adjust canvas size
	   stage.canvas.width = ow * scale;
	   stage.canvas.height = oh * scale;
	}
	
	stage.update()
	//this.replayAnimation();
	}

}
var _extends = function(ChildClass, ParentClass)
{
    var f = function() { };
    f.prototype = ParentClass.prototype;

    // copy child prototype methods in case there are some defined already
    for(var m in ChildClass.prototype)
    {
        f.prototype[m] = ChildClass.prototype[m];
    }

    ChildClass.prototype = new f();
    ChildClass.prototype.constructor = ChildClass;
    ChildClass.prototype._super = ParentClass.prototype;        
};
var SoundWave = function()
{
	this.loopCount = 0;
	this._super.constructor.call(this);
}
SoundWave.prototype.createBg = function(color,rot,inc)
{

	
   // this.rotation-=130
	this.maskedC = new createjs.Container();
	this.bg = new createjs.Shape();
    this.bg.graphics.beginFill(color).drawCircle(0,0,75);
    this.bg.alpha=.4;
    
    //this.alpha = .2;

    var pie = new createjs.Shape();
	pie.setMyAngle = .75;
    var num = 75;
    pie.graphics.beginFill(color).arc(0, 0, num, 0, Math.PI * pie.setMyAngle, false).lineTo(0, 0).closePath();
   // this.addChild(pie);
    //this.alpha = .2;
    
    this.circlesAr = new Array();
    this.circleRadiusAr = new Array();
    this.circleRadiusAr = [30,50,70];
    this.circleSpeedAr = [10,50,100];
    this.delay = inc;
    for(var i = 0;i<3;i++)
    {
    	var temp = new createjs.Shape();
	    temp.graphics.setStrokeStyle(3);
	 	temp.graphics.beginStroke("#ffffff");
	 	temp.graphics.drawCircle(0,0,this.circleRadiusAr[i]);
	 	this.maskedC.addChild(temp);
	 	temp.alpha = 0;
	 	this.circlesAr.push(temp);
	 	
	         

    }
    
    //s
    this.addChild(this.maskedC);
    this.maskedC.addChild(this.bg);
    this.maskedC.mask = pie;
    this.rotation =rot;
    this.sequenceLoop();
}
SoundWave.prototype.sequenceLoop = function()
{
	
	var tweenSubj = this.circlesAr[this.loopCount];
	tweenSubj.alpha = 0
	//debugger
	createjs.Tween.get(tweenSubj)
	 	  .to({alpha:.6},300,createjs.Ease.linear)
	 	  .wait(10*this.delay)
	 	  .to({alpha:0},150,createjs.Ease.linear)
	 	  .call(this.sequenceLoop,[],this)
	 	  	this.loopCount ++;
	 	  if(this.loopCount>2)
	 	  {
	 	  	this.loopCount = 0;
	 	  }
}
_extends(SoundWave, createjs.Container);
