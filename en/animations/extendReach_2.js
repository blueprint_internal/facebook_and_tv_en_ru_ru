function ExtendReach_2(target,resources)
{
	this.target = target;
	this.spritesAr = new Array();
	this.soundWavesAr = new Array();
	this.resources = resources;
	this.setUpPreloader();
}

ExtendReach_2.prototype = {
	
	setUpPreloader: function()
	{
		this.keepAspectRatio = true;
		this.loadedGraphics = 0;
		canvas = document.getElementById("extendReach_2");
		stage = new createjs.Stage(canvas);
		stage.enableMouseOver(10);
		this.stage = stage
		borderPadding = 10;

		var barHeight = 20;
		loaderColor = createjs.Graphics.getRGB(61, 92, 155);
		loaderBar = new createjs.Container();

		bar = new createjs.Shape();
		bar.graphics.beginFill(loaderColor).drawRect(0, 0, 1, barHeight).endFill();

		imageContainer = new createjs.Container();
		imageContainer.x = 430;
		imageContainer.y = 200;

		loaderWidth = 300;
		stage.addChild(imageContainer);

		var bgBar = new createjs.Shape();
		var padding = 3
		bgBar.graphics.setStrokeStyle(1).beginStroke(loaderColor).drawRect(-padding / 2, -padding / 2, loaderWidth + padding, barHeight + padding);

		loaderBar.x = canvas.width - loaderWidth >> 1;
		loaderBar.y = canvas.height - barHeight >> 1;
		loaderBar.addChild(bar, bgBar);

		stage.addChild(loaderBar);

		manifest = 
		[
			{src: this.resources.image1, id: "image1"},
			{src: this.resources.image2, id: "image2"},
			{src: this.resources.image3, id: "image3"}

		];
		this.map = new Array();
		preload = new createjs.LoadQueue(true);
       

		preload.on("progress", this.handleProgress,this);
		preload.on("complete", this.handleComplete,this);
		preload.on("fileload", this.handleFileLoad,this);
		preload.loadManifest(manifest,true);
		createjs.Ticker.setFPS(30);
	},
	
	handleProgress: function(event) {
		bar.scaleX = event.loaded * loaderWidth;
	},

	handleComplete: function(event) {
		
		loaderBar.visible = false;
		this.setupAnimation();
	   	stage.update();
       	var self = this;
       	createjs.Ticker.addEventListener("tick",self.handleTick);
    },

    handleTick: function(event){
    	stage.update();
    },

	handleFileLoad: function(event) {
		this.map.push(new createjs.Bitmap(event.result))
		stage.update();
	},

    setupAnimation: function()
    {
    	this.delaysAr = new Array();
    	this.delaysAr = [300,2000,3000,1000];
    	this.alphaAmt = new Array();
    	this.alphaAmt = [1,1,1,1,1];
    	this.spritesAr=[this.map[0],this.map[1],this.map[2]];
		stage.addChild(this.spritesAr[0]);
		this.spritesAr[1].x = 125;
		this.spritesAr[1].y = 196;
		//
		stage.addChild(this.spritesAr[1]);
		stage.addChild(this.spritesAr[2]);
		

		var mask_1 = new createjs.Shape();
    	mask_1.graphics.beginFill('#ff0000').drawRect(0, 0, 650, 189);
    	mask_1.x = this.spritesAr[1].x;
    	mask_1.y = this.spritesAr[1].y;
		this.spritesAr[2].x = 125;
		this.spritesAr[2].y = 196;
		//
		var mask_2 = new createjs.Shape();
    	mask_2.graphics.beginFill('#000000').drawRect(0, 0, 650, 189);
    	mask_2.x = this.spritesAr[2].x;
    	mask_2.y = this.spritesAr[2].y;

    	this.textContainer = new createjs.Container();
    	this.spritesAr.push(this.textContainer);
    	stage.addChild(this.lidContainer);
	    var title = new createjs.Text(this.resources.title, "30px freight-sans-pro","#ffffff");
	    this.textContainer.addChild(title);
	    title.lineWidth = 658;
		this.textContainer.x=100;
		this.textContainer.y=50
		this.textContainer.origins = {x:this.textContainer.x,y:this.textContainer.y};
		title.textBaseline = "alphabetic";
		stage.addChild(this.textContainer);

		this.textContainer_2 = new createjs.Container();//
		var footNote = new createjs.Text(this.resources.footnote, "12px freight-sans-pro","#ffffff");
	    this.textContainer_2.addChild(footNote);
	    footNote.lineWidth = 300;
		this.textContainer_2.x=60;
		this.textContainer_2.y=410;
		stage.addChild(this.textContainer_2);
		stage.update();
    	this.playAnimation();
    	this.resources.footnote
 	},

    resetAnimation: function()
    {
    	console.log("reset animation")
    	var self = this;
       	createjs.Ticker.addEventListener("tick",self.handleTick);
       	for(var i=0;i<this.spritesAr.length;i++)
		{
			this.spritesAr[i].alpha = 0;
			createjs.Tween.get(this.spritesAr[i])
	         .wait(this.delaysAr[i])
	         .to({alpha:this.alphaAmt[i]}, 1000,createjs.Ease.linear)
		}
    },

	playAnimation: function()
	{
		this.textContainer.alpha = 0;
		for(var i=0;i<this.spritesAr.length;i++)
		{

			this.spritesAr[i].alpha = 0;
			createjs.Tween.get(this.spritesAr[i])
	         .wait(this.delaysAr[i])
	         .to({alpha:this.alphaAmt[i]}, 1000,createjs.Ease.linear)
		}
	},

	animationComplete: function(event)
	{
		var self = this;
       	createjs.Ticker.removeEventListener("tick",self.handleTick);
	},
	resize: function(w,h)
	{
		var ow = 858; // your stage width
		var oh = 457; // your stage height

	if (this.keepAspectRatio == true)
	{
	    // keep aspect ratio
	    var scale = Math.min(w / ow, h / oh);
	    stage.scaleX = scale;
	    stage.scaleY = scale;

	   // adjust canvas size
	   stage.canvas.width = ow * scale;
	   stage.canvas.height = oh * scale;
	}
	
	stage.update()
	//this.replayAnimation();
	}

}
