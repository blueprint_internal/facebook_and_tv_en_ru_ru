
function ReachSpheres(resources)
{
	ReachSpheres.resources = resources;
}
ReachSpheres.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(480, 450, Phaser.CANVAS, 'reach_spheres', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this },null,null,false);
	},

	preload: function()
	{
		
		this.game.scale.maxWidth = 480;
    	this.game.scale.maxHeight = 450;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		
		this.game.load.image('laptop', ReachSpheres.resources.laptop);
        this.game.load.image('laptop_center', ReachSpheres.resources.laptop);
		this.game.load.image('screens', ReachSpheres.resources.screens);
        this.game.load.image('screen_center', ReachSpheres.resources.screens);
		this.game.load.image('circle_left', ReachSpheres.resources.circle_left);
		this.game.load.image('circle_right', ReachSpheres.resources.circle_right);
        this.game.load.image('circle_blend', ReachSpheres.resources.circle_right);
		
		
		this.game.created = false;
    	
    	
    	
  
    	this.game.stage.backgroundColor = '#ffffff';
    	//0f2747
    	
	},

	create: function(evt)
	{
		
		if(this.game.created === false)
		{
			
			this.parent.laptop = this.game.make.sprite(0,0,'laptop');
			this.parent.laptop.anchor.set(0.5,0.5);
            
            this.parent.laptopCenter = this.game.make.sprite(25,0,'laptop_center');
			this.parent.laptopCenter.anchor.set(0.5,0.5);

			this.parent.screens = this.game.make.sprite(0,0,'screens');
			this.parent.screens.anchor.set(0.5,0.5);
            
            this.parent.screenCenter = this.game.make.sprite(-25,0,'screen_center');
			this.parent.screenCenter.anchor.set(0.5,0.5);

			this.parent.circle_right = this.game.make.sprite(0,0,'circle_right');
			this.parent.circle_right.anchor.set(0.5,0.5);
            this.parent.circle_right.alpha = .7;
            
            this.parent.circle_blend= this.game.make.sprite(0,0,'circle_right');
			this.parent.circle_blend.anchor.set(0.5,0.5);
            
            

			this.parent.circle_left = this.game.make.sprite(0,0,'circle_left');
			this.parent.circle_left.anchor.set(0.5,0.5);

			this.parent.game.stage.backgroundColor = '#ffffff';
			
	    	this.game.created  = true;
	    	this.parent.buildAnimation();
	    }
	},
    
	buildAnimation: function()
	{
		var style = ReachSpheres.resources.textStyle_1;
		var style2 = ReachSpheres.resources.textStyle_2;
        var style3 = ReachSpheres.resources.textStyle_3;
        var style4 = ReachSpheres.resources.textStyle_4;
        
		this.leftSphereGroup =  this.game.add.group();
		this.leftSphereGroup.add(this.circle_left);
        this.screens.x-=70;
        
		this.leftSphereGroup.add(this.screens);
		this.leftSphereGroup.origins = {x:170,y:this.game.world.centerY+10}
		this.leftSphereGroup.x = this.leftSphereGroup.origins.x;
		this.leftSphereGroup.y = this.leftSphereGroup.origins.y;
		this.leftSphereGroup.alpha = 0;

		this.rightSphereGroup =  this.game.add.group();
		this.rightSphereGroup.origins = {x:310,y:this.game.world.centerY+10}
		this.rightSphereGroup.x = this.rightSphereGroup.origins.x;
		this.rightSphereGroup.y = this.rightSphereGroup.origins.y;
        this.rightSphereGroup.add(this.circle_blend);
        this.circle_blend.blendMode = PIXI.blendModes.LIGHTEN;
		this.rightSphereGroup.add(this.circle_right);
		this.rightSphereGroup.add(this.laptop);
        this.laptop.x+=70;
		this.rightSphereGroup.alpha = 0;


		this.arrowGroup_1 = this.game.add.group();
		this.arrowGroup_2 = this.game.add.group();
		this.centerGroup = this.game.add.group();
		this.text_top= this.game.make.text(0,0, ReachSpheres.resources.text_top,style3);
		this.text_top.anchor.set(0.5,0.5);

		
		this.arrowGroup_1.origins = {x:this.game.world.centerX,y:30};
		this.arrowGroup_1.x = this.arrowGroup_1.origins.x;
		this.arrowGroup_1.y = this.arrowGroup_1.origins.y;
		
		
		

		
		this.arrowGroup_1.add(this.text_top);
		this.arrowGroup_1.alpha = 0;
		
		
		this.arrowGroup_2.origins = {x:this.game.world.centerX,y:390};
		this.arrowGroup_2.x = this.arrowGroup_2.origins.x;
		this.arrowGroup_2.y = this.arrowGroup_2.origins.y;
		this.text_bottom = this.game.make.text(0,0, ReachSpheres.resources.text_bottom,style3);
		this.text_bottom.anchor.set(0.5,0.5);
		this.arrowGroup_2.addChild(this.text_bottom);
        
        this.percent_center = this.game.make.text(0,-50, ReachSpheres.resources.percent_center,style);
        this.percent_center.anchor.set(0.5,0.5);
        
		this.centerGroup.x = this.game.world.centerX;
        this.centerGroup.y = this.game.world.centerY+10;
        this.centerGroup.add(this.laptopCenter);
		this.centerGroup.add(this.screenCenter);
        this.centerGroup.add(this.percent_center);
        this.centerGroup.alpha = 0;
       // this.centerGroup.x
		this.arrowGroup_2.alpha = 0;

		this.leftGroup = this.game.add.group();
		this.leftGroup.origins = {x:120,y:200};
		
		this.text_left = this.game.make.text(0,0, ReachSpheres.resources.text_left,style);
        
        
		this.percent_left = this.game.make.text(-70,-50, ReachSpheres.resources.percent_left,style);
        this.percent_left.anchor.set(0.5,0.5);
		this.leftSphereGroup.add(this.percent_left);
        
        this.text_left = this.game.make.text(-70,-25, ReachSpheres.resources.text_left,style2);
        this.text_left.anchor.set(0.5,0.5);
		this.leftSphereGroup.add(this.text_left);
        
        this.leftSphereGroup.add(this.text_left);
        
        this.percent_right = this.game.make.text(70,-50, ReachSpheres.resources.percent_right,style);
        this.percent_right.anchor.set(0.5,0.5);
		this.rightSphereGroup.add(this.percent_right);
        this.text_right = this.game.make.text(70,-25, ReachSpheres.resources.text_left,style2);
        this.text_right.anchor.set(0.5,0.5);
        this.rightSphereGroup.add(this.text_right);
		
        this.text_center = this.game.make.text(0,-25, ReachSpheres.resources.text_center,style2);
        this.text_center.anchor.set(0.5,0.5);
        this.centerGroup.add(this.text_center);
        
        this.percent_bottom = this.game.make.text(0,30, ReachSpheres.resources.percent_bottom,style4);
        this.percent_bottom.anchor.set(0.5,0.5);
        this.arrowGroup_2.addChild(this.percent_bottom);
        
        
        
		this.arrowGroup_1.y=-120;
		this.arrow_1_anim = this.game.add.tween(this.arrowGroup_1).to({alpha:1,y: this.arrowGroup_1.origins.y},900,Phaser.Easing.Quadratic.Out);
		//this.arrow_1_anim.start();
		this.leftSphereGroup.x = -250;
		this.rightSphereGroup.x = 1150;
		this.arrowGroup_2.y = 500;
		this.leftSphere_anim = this.game.add.tween(this.leftSphereGroup).to({alpha:1,x: this.leftSphereGroup.origins.x},1000,Phaser.Easing.Quadratic.Out);
		this.rightSphere_anim = this.game.add.tween(this.rightSphereGroup).to({alpha:1,x: this.rightSphereGroup.origins.x},1000,Phaser.Easing.Quadratic.Out);
		this.centerGroup_anim = this.game.add.tween(this.centerGroup).to({alpha:1},1000,Phaser.Easing.Quadratic.Out);
		
		this.arrow_2_anim = this.game.add.tween(this.arrowGroup_2).to({alpha:1,y: this.arrowGroup_2.origins.y},900,Phaser.Easing.Quadratic.Out);

		this.leftSphere_anim.chain(this.rightSphere_anim,this.centerGroup_anim,this.arrow_2_anim,this.arrow_1_anim);
		this.leftSphere_anim.start();

	},
	inview: function()
	{
		
		
	},

	animate: function()
	{
		//console.log("animate")
	},

	update: function()
	{

	},
	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}
//course/en/animations/nielsen_audience/



